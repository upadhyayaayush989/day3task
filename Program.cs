﻿using MethodOverloading.Repository;
using MethodOverloading.Model;


ProductRepository productRepository = new ProductRepository();
Product[] allProducts = productRepository.GetAllProducts();
foreach(Product item in allProducts)
{
    if (item.Category == "mobile")
    {
        Console.WriteLine($"Name: {item.Name}\nCategory: {item.Category}\nRating: {item.Rating}");
    }
}
Console.WriteLine("Do you want to update the Product Y:yes , N:no");
string ans = Console.ReadLine();

if(ans == "Y")
{
    Console.WriteLine("Enter the index::");
    int ind = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter the Name::");
    string name1 = Console.ReadLine();

    Console.WriteLine("Enter the Category::");
    string categ = Console.ReadLine();

    Console.WriteLine("enter the price::");
    int price1 = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter the Rating::");
    int rating1 = int.Parse(Console.ReadLine());

    productRepository.UpdateProduct(ind, name1, categ, price1, rating1);
}
Console.WriteLine("Enter index to delete ");
productRepository.DeleteProduct(int.Parse(Console.ReadLine()));

Product[] allProducts1 = productRepository.GetAllProducts();
foreach (Product item in allProducts1)
{

   Console.WriteLine($"Name: {item.Name}  Category: {item.Category}  Rating: {item.Rating}");
    
}